from tkinter import *
from tkinter.filedialog import asksaveasfilename, askopenfilename
import subprocess,os
#from ttkthemes import themed_tk as tk
#import ttk

compiler = Tk()
#compiler = tk.ThemedTk()
#compiler.get_themes()
#compiler.set_theme("plastik")
compiler.title('gcode')
file_path = ''

def makefile(file):
	os.system(f"touch {file}")

def shell():
	# Linux
	"""Please uncomment the terminal you use"""
	#os.system("qterminal")
	os.system("gnome-terminal")
	#os.system("xfce4-terminal")
	#os.system("konsole")
	#os.system("kitty")
	#os.system("allactrity")
	#os.system("xterm")
	
	# Windows
	#os.system("powershell")
	#os.system("cmd")

def edit():
	os.system("python /home/$USER/Programming/gcompiler/src/trial.py")

def set_file_path(path):
    global file_path
    file_path = path


def open_file():
    path = askopenfilename(filetypes=[('All files', '*.*')])
    with open(path, 'r') as file:
        code = file.read()
        editor.delete('1.0', END)
        editor.insert('1.0', code)
        set_file_path(path)


def save_as():
    if file_path == '':
        path = asksaveasfilename(filetypes=[('All files', '*.*')])
    else:
        path = file_path
    with open(path, 'w') as file:
        code = editor.get('1.0', END)
        file.write(code)
        set_file_path(path)


def run_c():
    if file_path == '':
        save_prompt = Toplevel()
        text = Label(save_prompt, text='Please save your code before running it.')
        text.pack()
        return
    command = f'tcc -run {file_path}'
    process = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
    output, error = process.communicate()
    code_output.insert('1.0', output)
    code_output.insert('1.0',  error)

def run_py():
    if file_path == '':
        save_prompt = Toplevel()
        text = Label(save_prompt, text='Please save your code before running it.')
        text.pack()
        return
    command = f'python3 {file_path}'
   # Python3 support
    process = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
    output, error = process.communicate()
    code_output.insert('1.0', output)
    code_output.insert('1.0',  error)
    
def run_go():
    if file_path == '':
        save_prompt = Toplevel()
        text = Label(save_prompt, text='Please save your code before running it.')
        text.pack()
        return
    command = f'go run {file_path}'
    process = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
    output, error = process.communicate()
    code_output.insert('1.0', output)
    code_output.insert('1.0',  error)

def run_jv():
    if file_path == '':
        save_prompt = Toplevel()
        text = Label(save_prompt, text='Please save your code before running it.')
        text.pack()
        return
    command = f'java {file_path}'
    process = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
    output, error = process.communicate()
    code_output.insert('1.0', output)
    code_output.insert('1.0',  error)
    
def run_js():
    if file_path == '':
        save_prompt = Toplevel()
        text = Label(save_prompt, text='Please save your code before running it.')
        text.pack()
        return
    command = f'node {file_path}'
    process = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
    output, error = process.communicate()
    code_output.insert('1.0', output)
    code_output.insert('1.0',  error)

menu_bar = Menu(compiler)

def plugin_mgr_loader():
	if __name__ == "__main__":
		os.system("python3 /home/$USER/Programming/gcompiler/src/plugin-manager.py")
	else:
		for i in range(1):
			pass

for  i in range(1):
	file_menu = Menu(menu_bar, tearoff=0)
	file_menu.add_command(labe='New file',command=makefile)
	file_menu.add_command(label='Open', command=open_file)
	file_menu.add_command(label='Save', command=save_as)
	file_menu.add_command(label='Save As', command=save_as)
	file_menu.add_command(label='Exit', command=exit)
	menu_bar.add_cascade(label='File', menu=file_menu)

for i in range(1):
	run_bar = Menu(menu_bar, tearoff=0)
	run_bar.add_command(label='Run C file', command=run_c)
	run_bar.add_command(label='Run Python file', command=run_py)
	run_bar.add_command(label='Run JavaScript file', command=run_go)
	run_bar.add_command(label='Run Go file', command=run_go)
	run_bar.add_command(label='Run Java file', command=run_go)
	menu_bar.add_cascade(label='Run', menu=run_bar)

for i in range(1):
	shell_bar = Menu(menu_bar, tearoff=0)
	shell_bar.add_command(label='New Terminal', command=shell)
	shell_bar.add_command(label='Split Terminal', command=shell)
	shell_bar.add_command(label='Python prompt', command=shell)
	shell_bar.add_command(label='Nodejs prompt', command=shell)
	for i in range(1):
		menu_bar.add_cascade(label='Terminal', menu=shell_bar)

for i in range(1):
	edit_bar = Menu(menu_bar, tearoff=0)
	edit_bar.add_command(label='Settings', command=edit)
	edit_bar.add_command(label='Copy', command=edit)
	edit_bar.add_command(label='Paste', command=edit)
	edit_bar.add_command(label='Cut', command=edit)
	edit_bar.add_command(label='Color scheme', command=edit)
	edit_bar.add_command(label='Install Plugins', command=plugin_mgr_loader)
	edit_bar.add_command(label='Color scheme', command=edit)
	menu_bar.add_cascade(label='Edit', menu=edit_bar)
	for i in range(1):
		compiler.config(menu=menu_bar)


for i in range(1):
	editor = Text()
	editor.pack()

for i in range(1):
	code_output = Text(height=10)
	code_output.pack()

if __name__ == "__main__":
	for i in range(1):
		compiler.mainloop()

else:
	for i in range(1):
		pass

