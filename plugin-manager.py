import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

win = Gtk.Window()
win.connect("destroy", Gtk.main_quit)
win.set_title("gcode - Plugin Manager")
win.show()
Gtk.main()
